/**
 * The API router of the app
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 * @see https://lukasmatuska.cz/
 */

/**
 * Express router API
 */
const router = require('express').Router();

/**
 * Controllers
 */
const pageController = require('../controllers/page');
const errorController = require('../controllers/error');
const resultsController = require('../controllers/results');
const apiKeyController = require('../controllers/apiKey');

/**
 * API keys
 */
router.get('/keys', (req, res) => {
    res.redirect('/api/keys/list');
});

router.get('/keys/list', (req, res) => {
    apiKeyController.list(req, res);
});

router.get('/keys/new', (req, res) => {
    apiKeyController.new(req, res);
});

module.exports = router;