/**
 * The entry router of the app
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 * @see https://lukasmatuska.cz/
 */

/**
 * Express router API
 */
const router = require('express').Router();

/**
 * Controllers
 */
const pageController = require('../controllers/page');
const errorController = require('../controllers/error');

router.get('/', (req, res) => {
    res.redirect('/login');
});

router.get('/login', (req, res) => {
    pageController.loginPage(req, res);
});

router.get('*', (req, res) => {
    errorController.error404(req, res);
});

module.exports = router;