/**
 * Error controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */

/**
 * Models
 */

module.exports.error403 = (req, res) => {
   return res.render('error/accessDenied', { req, res });
};

module.exports.error404 = (req, res) => {
   return res.render('error/notFound', { req, res });
};