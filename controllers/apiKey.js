/**
 * Results controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
const moment = require('moment');
const randomstring = require('randomstring');

/**
 * Models
 */
const ApiKey = require('../models/ApiKey');

module.exports.new = (req, res) => {
   new ApiKey({
      //author: (req.session.user ? req.session.user._id : null),
      public: randomstring.generate(),
      private: randomstring.generate(),
   }).save((err, apiKey) => {
      if (err) {
         return console.error(err);
      }
      delete apiKey._id;
      delete apiKey.__v;
      return res.send(apiKey);
   });
};

module.exports.list = (req, res) => {
   ApiKey.find({
      //year: req.session.year,
   }, (err, apiKeys) => {
      if (err) {
         res.send(err);
         return console.error(err);
      }
      for (let i = 0; i < apiKeys.length; i++) {
         delete apiKeys[i].secret;
      }
      return res.send(apiKeys);
   });
};