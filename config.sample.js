module.exports = {
    // location
    protocol: 'http',
    url: 'localhost',
    port: 3000,

    // full url
    fullUrl: this.protocol + '://' + this.url + (String(this.port).length > 0 ? ':' + this.port : ''),

    // database credentials
    db: {
        port: 27017,
        host: 'localhost',
        name: 'tutorial',
        user: 'username',
        password: 'password',
    },
};