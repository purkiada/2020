/**
 * User database model
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
const moment = require('moment');
moment.locale('cs');
// library for easy database manipulations
const mongoose = require('../libs/db');

// the schema itself
var apiKeySchema = new mongoose.Schema({
    author: {
        type: String,
    },
    public: {
        type: String,
    },
    private: {
        type: String,
    },
});

// export
module.exports = mongoose.model('apiKey', apiKeySchema, 'apiKey');